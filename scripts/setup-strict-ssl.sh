#!/bin/bash
# these recommndations come from
# https://ssl-config.mozilla.org/#server=apache&version=2.4.29&config=intermediate&openssl=1.1.1&guideline=5.6

echo " "
echo "###################################################################################"
echo "Adding strict SSL support"
echo "###################################################################################"
echo " "

cat >> /etc/apache2/mods-available/ssl.conf  << EOF

#recommendations taken from https://ssl-config.mozilla.org/
SSLProtocol             all -SSLv3 -TLSv1 -TLSv1.1
SSLCipherSuite          ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384
SSLHonorCipherOrder     off
SSLSessionTickets       off

SSLUseStapling On
SSLStaplingCache "shmcb:logs/ssl_stapling(32768)"
EOF