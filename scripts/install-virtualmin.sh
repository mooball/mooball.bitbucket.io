#!/bin/bash
echo " "
echo "###################################################################################"
echo "Perl modules needed for Virtualmin CSF and AWS backups"
echo "###################################################################################"
echo " "

apt-get -o DPkg::Options::=--force-confdef -y install liblwp-protocol-https-perl
apt-get -o DPkg::Options::=--force-confdef -y install libxml-simple-perl
apt-get -o DPkg::Options::=--force-confdef -y install libdigest-hmac-perl
apt-get -o DPkg::Options::=--force-confdef -y install libconfig-inifiles-perl

echo " "
echo "###################################################################################"
echo "Install Virtualmin"
echo "###################################################################################"
echo " "

wget http://software.virtualmin.com/gpl/scripts/install.sh -O /root/virtualmin-install.sh
sh /root/virtualmin-install.sh -m -y -n $HOSTNAME

echo " "
echo "###################################################################################"
echo "Installing CSF"
echo "###################################################################################"
echo " "

# I prefer CSF so remove FirewallD
apt autoremove firewalld -y
# Download, extract, install CSF, add module to Webmin
cd /root; wget https://download.configserver.com/csf.tgz
tar -xzf csf.tgz
cd csf
sh install.sh
/usr/share/webmin/install-module.pl /etc/csf/csfwebmin.tgz
