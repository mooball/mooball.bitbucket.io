#!/bin/bash
useradd -m -d /home/sergey -s /bin/bash sergey
usermod -aG wheel sergey
usermod -aG sudo sergey
mkdir /home/sergey/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDO9Du6djbJV04EhPHFrJrXl1tjrSGLd3GQoyuyqOTO1MYS8xR2CjSNIF6wmCBrGGZkkVv3OuLcQhPkJIXaKoi+NlIFIN/MyOEDqoiPN6QS/neIzEMvSAOoJjhpBroKF64f8/JH7RhmYwh48ioy0AeQibXg5g8Zk3tJqXEIuY9lZjKDHD0aanEsB0E3gIA9CTwfS7Uhm/8d6DW4aRDHlkHhdlB2BkjebL9edtXpWtOSKEP7gGON6QODiA5OieHpbK0J1UNub4h/Y9I0MGXnKjAoNgCR5W3GNldjY41Im//L17M1ioPz5RQNorBDE4q+blzMdE/Zy4KN3x7Lx3fiSX9Y5INw25dRVPdLD4OW+acZ5wFFQDybn80c7fAOgU6l9D+TCCDoyz1U/dVQ5zgsn2szJ872SMAZNkk7tLu1zsa0iiMxcM2HVoVRqK+abNTsg2IceohEb8WwSCaFLP0iCLM+EGm2HtqDHwDjq4b75TpAeCfF1l0DfYmf9hV9/fIZWxLTKU9BCxjeW5NFZwdr63tgbS9GrX7SPiDBZ/dhlQuuTDVDWGwnrPBpKWCl39s2eG6p8QRWSiQMhb3n21UZWBNU7Xr4Sal45INvA5Ch41Clszq4llx2YpSND/g9q1y7ZcgxV8T6BL+xzBq+KM/VY/zyW6f1AUU+md//dUDksxBk4Q== sergey@sergey-desktop" >> /home/sergey/.ssh/authorized_keys
chown -R sergey:sergey /home/sergey/.ssh
chmod 700 /home/sergey/.ssh
chmod 600 /home/sergey/.ssh/authorized_keys
