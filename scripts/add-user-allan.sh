#!/bin/bash
useradd -m -d /home/john -s /bin/bash john
usermod -aG wheel john
usermod -aG sudo john
mkdir /home/john/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDa0TjDsxLVrsaz+xFpSl5JHsbYgwwbclZPDq9yv2Pr+9ca/XrLIioQHTg/6SZXD++SyL8090lri2WLncxOZoZ1TIBi2refE/8BGxcGefV9NghPbw2CVzmlqhtFaFYkE/NaaRIH396Me5gUFYNBFH05QQohG20fRFRWKHrXAOluAQex86mvpFvm65jbskMc26Q+iCjDlj4zdE5VjNBMVUTOw0ZDnn7sIabAMy3a3HnXd/O6NBW8NRm38InKItTswiNPdAfW3oB0DxTnnTnddUId6l/n0wpRydG31iAZm2OZUEzBvoAjaFnz7gCO+9UwB+daai5azbR0FDNp8XvYQvML JohnRemartA@kickseed" >> /home/john/.ssh/authorized_keys
chown -R john:john /home/john/.ssh
chmod 700 /home/john/.ssh
chmod 600 /home/john/.ssh/authorized_keys
