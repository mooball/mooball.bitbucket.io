#!/bin/bash

echo " "
echo "###################################################################################"
echo "Install mailhog"
echo "###################################################################################"
echo " "

# get latest Mailhog binary and install
wget https://github.com/mailhog/MailHog/releases/download/v1.0.1/MailHog_linux_amd64
sudo cp MailHog_linux_amd64 /usr/local/bin/mailhog
sudo chmod +x /usr/local/bin/mailhog

# get latest mhsendmail binary and install
wget https://github.com/mailhog/mhsendmail/releases/download/v0.2.0/mhsendmail_linux_amd64
sudo cp mhsendmail_linux_amd64 /usr/local/bin/mhsendmail
sudo chmod +x /usr/local/bin/mhsendmail

# set up systemctl file

sudo tee /etc/systemd/system/mailhog.service <<EOL
[Unit]
Description=Mailhog
After=network.target
[Service]
User=root
ExecStart=/usr/bin/env /usr/local/bin/mailhog -api-bind-addr 127.0.0.1:8025 -ui-bind-addr 127.0.0.1:8025 -smtp-bind-addr 127.0.0.1:1025 > /dev/null 2>&1 &
[Install]
WantedBy=multi-user.target
EOL

# force service to restart
systemctl daemon-reload
systemctl enable mailhog
systemctl restart mailhog
