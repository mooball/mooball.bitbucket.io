#!/bin/bash

echo " "
echo "###################################################################################"
echo "Install ManageSieve alongside procmail on a virtualmin server"
echo "Note - may need updating if virtualmin changes procmail rules in the future"
echo "###################################################################################"
echo " "


apt install dovecot-sieve dovecot-managesieved

# Enable protocol and listener
echo '
#
# Enabling protocol and listener
protocols = $protocols sieve
service managesieve-login {
    inet_listener sieve {
        port = 4190
    }
} ' >> /etc/dovecot/conf.d/20-managesieve.conf


# enable plugins
sed -i "s|protocol lda {|protocol lda {\n mail\_plugins = \$mail\_plugins sieve|" /etc/dovecot/conf.d/15-lda.conf


# add deliver to procmailrc so it still works
cp -a /etc/procmailrc{,.orig}

echo '
DELIVER=/usr/lib/dovecot/deliver
LOGFILE=/var/log/procmail.log
TRAP=/etc/webmin/virtual-server/procmail-logger.pl
:0wi
VIRTUALMIN=|/etc/webmin/virtual-server/lookup-domain.pl --exitcode 73 $LOGNAME
EXITCODE=$?
:0
* ?/usr/bin/test "$EXITCODE" = "73"
/dev/null
EXITCODE=0
:0
* ?/usr/bin/test "$VIRTUALMIN" != ""
{
INCLUDERC=/etc/webmin/virtual-server/procmail/$VIRTUALMIN
}
ORGMAIL=$HOME/Maildir/
DEFAULT=$HOME/Maildir/
DROPPRIVS=yes
:0w
| $DELIVER
:0
$DEFAULT ' > /etc/procmailrc


# restart
systemctl restart dovecot