#!/bin/bash
useradd -m -d /home/sam -s /bin/bash sam
usermod -aG wheel sam
usermod -aG sudo sam
mkdir /home/sam/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCzQ8OsTUQkHvY8yHJEqVQC7kEt0gvLs7nRcL3VbxNPFq0GMei56CA+8iN+zBV2a1rbkPRfIcWdb1mDHHbBgiiH++MJuYpKohB7BHY32UB98vBMWUwgIBYXWE+HodiMg3XC+4oTWEv2QX35cuBTg8Dy6A3Mc8odjd6NV1RC7B3Csq/2uD2E+DQFjBUNLpUvq2tWyN5pLKgpwBeAVBpQncWQWwjhnMlgmN57yu5sw0kVvMVzPBVRtu/Vio+rONxZYCHTE3Sg0I3+95lSsXHXdYc3ozsQ1Xnai2YcS2Eur11rUZ6DojpeZiyOUVJ1xQDDXR1uovimtub7/wcUq86hBIsL sam@sam-ubuntu" >> /home/sam/.ssh/authorized_keys
chown -R sam:sam /home/sam/.ssh
chmod 700 /home/sam/.ssh
chmod 600 /home/sam/.ssh/authorized_keys
