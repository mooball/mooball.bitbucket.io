#!/bin/bash

echo " "
echo "###################################################################################"
echo "Install PPAs for PHP and apache2"
echo "###################################################################################"
echo " "

apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 4F4EA0AAE5267A6C
add-apt-repository ppa:ondrej/php -y
add-apt-repository ppa:ondrej/apache2 -y
apt-get update -y
apt-get -o DPkg::Options::=--force-confdef -y upgrade --with-new-pkgs

echo "###################################################################################"
echo "Set up versions of PHP and common associated modules"
echo "###################################################################################"
echo " "

# apt install php5.6-{bcmath,bz2,cgi,cli,common,curl,fpm,gd,geoip,imagick,intl,json,ldap,memcache,mbstring,mcrypt,mysql,opcache,pgsql,pspell,readline,soap,xml,zip} -y

# apt install php7.2-{bcmath,bz2,cgi,cli,common,curl,fpm,gd,geoip,gmp,imagick,intl,json,ldap,memcache,mbstring,mcrypt,mysql,opcache,pgsql,pspell,readline,soap,xml,zip} -y

apt-get install php7.4-{bcmath,bz2,cgi,cli,common,curl,fpm,gd,geoip,gmp,imagick,intl,json,ldap,memcache,mbstring,mcrypt,mysql,opcache,pgsql,pspell,readline,soap,xml,zip} -y

apt-get install php8.1-{bcmath,bz2,cgi,cli,common,curl,fpm,gd,gmp,imagick,intl,ldap,memcache,mbstring,mcrypt,mysql,opcache,pgsql,pspell,readline,soap,xml,zip} -y

apt-get install php8.2-{bcmath,bz2,cgi,cli,common,curl,fpm,gd,gmp,imagick,intl,ldap,memcache,mbstring,mcrypt,mysql,opcache,pgsql,pspell,readline,soap,xml,zip} -y

apt-get install php8.3-{bcmath,bz2,cgi,cli,common,curl,fpm,gd,gmp,imagick,intl,ldap,memcache,mbstring,mysql,opcache,pgsql,pspell,readline,soap,xml,zip} -y

