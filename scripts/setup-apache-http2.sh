#!/bin/bash
echo " "
echo "###################################################################################"
echo "HTTP2 support for apache"
echo "###################################################################################"
echo " "

# this was only needed prior to 22.04 LTS.

sudo a2enmod headers
# Set up HTTP2 support
sudo a2enmod proxy_fcgi
sudo a2dismod php7.4
sudo a2dismod mpm_prefork
sudo a2enmod mpm_event
sudo a2enmod http2
sudo systemctl restart apache2
printf "\n#Enable http2\nProtocols h2 http/1.1\n" >> /etc/apache2/apache2.conf
