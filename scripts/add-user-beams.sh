#!/bin/bash
useradd -m -d /home/beams -s /bin/bash beams
usermod -aG wheel beams
usermod -aG sudo beams
mkdir /home/beams/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAp3bbAAff0wmHFscIrBPmZUuNSjXBlTUOnHlhZP6mEgOPf6fDUpMktB9Y2Xg0BjUT4evWYitaqrP6kvw27xdhkwqdyowdWq/4QvkLOKAVVN3vu7AsL+QRgOj7lGtDebwsK9nuk2gDYTtpDCJDoJNSGOyVj0UO6GLOOkudbcbQpNxTxFHxWN17WhjrMMqmzw/8Q4BWEH62pwyYv8qDqyk1miI7j4Etv3LuMLIcT/gs6rRV8WUgWolJcuiI0Aju5r+r+2XzlqLfToXT44KMg68VRWPZvBuq1aUctY6zrQ4z/snzkRhVcb9pHS64oYd/ZYpNUJelMvEztrQTHgCgT7LhiQ== tom@mooball.net" >> /home/beams/.ssh/authorized_keys
chown -R beams:beams /home/beams/.ssh
chmod 700 /home/beams/.ssh
chmod 600 /home/beams/.ssh/authorized_keys
