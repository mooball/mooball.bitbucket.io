#!/bin/bash

echo " "
echo "###################################################################################"
echo "Give sudo group sudo access with no password"
echo "###################################################################################"
echo " "

echo "%sudo  ALL = (ALL) NOPASSWD: ALL" >> /etc/sudoers.d/sudo-group
chmod 0440 /etc/sudoers.d/sudo-group