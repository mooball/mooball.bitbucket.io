#!/bin/bash

echo " "
echo "###################################################################################"
echo "Install Mod_Pagespeed"
echo "###################################################################################"
echo " "

cd /root/
wget https://dl-ssl.google.com/dl/linux/direct/mod-pagespeed-stable_current_amd64.deb
dpkg -i mod-pagespeed-stable_current_amd64.deb
systemctl restart apache2